import React from 'react';
import { Nav, NavItem, NavLink,Form, FormGroup, Input, Button, Badge} from 'reactstrap';
import axios from 'axios';


class Header extends React.Component {
  constructor(props,context){
    super(props);
    
  }

  render() {
    return(
        <header>
          <FixedHeader></FixedHeader>
          <Menu></Menu>
        </header>
      );
  }
}

class FixedHeader extends React.Component{
  state={
    elemCart:0
  }
  constructor(props){
    super(props);

  }
  componentWillMount(){
   

  }
  componentDidMount(){
    /*var carrello=window.localStorage.getItem("carrello");
    if(carrello){
      var carrelloVett=carrello.split("_");
      this.state.elemCart=carrelloVett.length;
      
    }*/
    
    window.addEventListener('addToCart', (event) => {
      //this.setState({ products: [...this.state.products, event.detail] });
      var n=this.state.elemCart+1;
      this.setState({elemCart:n})
      //window.localStorage.setItem('carrello',window.localStorage.getItem("carrello")+"_"+event.detail);
      //console.log("evento arrivato"+event.detail);
    }, false);
  }
render(){
  
  
  return(
   <div className="fixedHeader">
    <div className="container">
      <div className="row">
        <div className="col-lg-4" style={{ padding: "0px"}}>
          <Nav className="navbar-left">
            <NavItem>
              <NavLink href="#" className="privati">Privati</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="#" className="impresa">Impresa Semplice</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="#">Chi Siamo</NavLink>
            </NavItem>
          </Nav>
        </div>
        <div className="col-lg-5" style={{ padding: "0px"}}>
        <Form className="ricerca">
          <FormGroup>
            <Input type="text" name="text" id="ricerca" className="inputSearch" placeholder="Di cosa hai bisogno?" />
            <a href="#" className="ico-cerca"><img src="https://img.tim.it:443/img/ico_cerca.png" /></a>
          </FormGroup>
        </Form>
        </div>
        <div className="col-lg-3 pull-right" style={{ padding: "0px"}}>
         <Nav className="navbar-right">
          <NavItem>
          
 {(this.state.elemCart!=0)? (<Badge color="danger">{this.state.elemCart}</Badge>):(<Badge color="danger"></Badge>)} <Button  bsStyle="link" className="ico_carrello nobordo"  onClick={openCart( true)}>carrello</Button>
          </NavItem>
          <NavItem>
            <NavLink href="#" className="ico_mail">Mail</NavLink>
          </NavItem>
          </Nav>
          </div>
      </div>
    </div>
    </div>
    )
}
}


class Menu extends React.Component{
 state = {
        vociMenu: []
        
    }
      constructor(props){
        super(props);
      }
      componentDidMount(){
        
          axios
            //.get(process.env.REACT_APP_CMS_URL+'/cockpit-master/api/collections/get/MENU?token=193f5bf31f4d33b0b06f76d90b462c')
            .get('http://jcmsb.hostinggratis.it/cockpit-master/api/collections/get/MENU?token=193f5bf31f4d33b0b06f76d90b462c')
            .then(
                response=>{
                    console.log(response.data.entries);
                    this.setState({vociMenu: response.data.entries});
                }
            )
      }


      render(){
        const menu= this.state.vociMenu.map((voce_menu,index)=>{
         return(<ListaVoci prod={voce_menu} key={index}></ListaVoci>)  
      });

      return(
        <div className="animateHeader">
          <div className="container">
            <a href="#" className="logo-tim"><img src="https://img.tim.it:443/img/logo_tim_2016.png" /></a>
            <Nav className="link-sito">
                {menu}
                <NavItem className="MyTim">
                  <NavLink className="mytim" href="#">MyTIM</NavLink>
                </NavItem>
            </Nav>
          </div>
        </div>
      );
  }
}


class ListaVoci extends React.Component{
      constructor(props){
          super(props);
         
      }
      render(){
      console.log(process.env);

      return(
      <NavItem className="offerte">
        <NavLink href="#">{this.props.prod.Description}</NavLink>
        <div className="cont-prodotti cont_menu">
            <div className="container">
              <div className="row">
                 <div className="col-lg-3 cont-banner" style={{ padding: "0px"}}>
                    <h5>Consigliato per te</h5>
                    <a href="#"><img src={this.props.prod.contenuto.img} /></a>
                  </div>
                  <div className="col-lg-9" style={{ padding: "0px"}}>
                     <h5>{this.props.prod.contenuto.titolo}</h5>
                      <div className="row lista-item">
                        <div className="col-lg-2">
                          <a href="#"> <img src={this.props.prod.contenuto.testo_icona[0].logo} />
                            <p>{this.props.prod.contenuto.testo_icona[0].testo}</p></a>
                        </div>
                        <div className="col-lg-2">
                          <a href="#"> <img src={this.props.prod.contenuto.testo_icona[1].logo} />
                            <p>{this.props.prod.contenuto.testo_icona[1].testo}</p></a>
                        </div>
                        <div className="col-lg-2">
                          <a href="#"> <img src={this.props.prod.contenuto.testo_icona[2].logo} />
                            <p>{this.props.prod.contenuto.testo_icona[2].testo}</p></a>
                        </div>
                        <div className="col-lg-2">
                          <a href="#"><img src={this.props.prod.contenuto.testo_icona[3].logo} />
                            <p>{this.props.prod.contenuto.testo_icona[3].testo}</p></a>
                        </div>
                        <div className="col-lg-2">
                          <a href="#"><img src={this.props.prod.contenuto.testo_icona[4].logo} />
                            <p>{this.props.prod.contenuto.testo_icona[4].testo}</p></a>
                        </div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
       </NavItem>
      )
    }
}
const openCart = (item) => () => {
  
  const event = new CustomEvent('openCart', { detail: item });
  window.dispatchEvent(event);
}

export default Header;

